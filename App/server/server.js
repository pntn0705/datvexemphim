import express from 'express';
import mongoose from 'mongoose';
import cors from 'cors';
import socketio from 'socket.io';
import http from 'http';
import Router from './routes/index.js';
import session from 'express-session';
import cookieParser from 'cookie-parser';

const app = express();

app.use(express.json());
app.use(cookieParser());
const PORT = process.env.PORT || 69;
const server = http.createServer(app);
const io = socketio(server);
app.use(cors());

const __CONNECTION_STRING = "mongodb+srv://admin:iysCVQpmZ7vhdZxx@cluster0.bhk4p.mongodb.net/DVXP?retryWrites=true&w=majority";

mongoose.connect(__CONNECTION_STRING,{
    useCreateIndex: true,
    useNewUrlParser: true,
    useUnifiedTopology: true
});

io.on('connection', (socket) => {
    console.log("we have connection");
    socket.on('disconnect', () => {
        console.log('user had left');
    })
});

app.use(session({
    secret: 'secret',
    resave: true,
    saveUninitialized: false,
    cookie: { secure: true, maxAge: 10000 }
}));

app.use('/', Router);

server.listen(PORT, () => console.log(`server started on port ${PORT}`));