import mongoose from 'mongoose';

const RoomSchema = mongoose.Schema({
    Name: String,
    TotalSeats: Number
});

export default mongoose.model('rooms', RoomSchema);