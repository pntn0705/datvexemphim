import mongoose from 'mongoose';

const filmSchema = mongoose.Schema({
    Name: String,
    Directors: String,
    Casts: String,
    GENRE: String,
    Running_Time: Number,
    Language: String,
    ReleaseDate: Date,
    Details: String,
    Trailer: String,
    Image: String
});

export default mongoose.model('films', filmSchema);