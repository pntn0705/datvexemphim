import mongoose from 'mongoose';

const commentSchema = mongoose.Schema({
    UserId: {type:mongoose.Schema.Types.ObjectId},
    FilmId: {type:mongoose.Schema.Types.ObjectId},
    Content: String,
    CreatedTime: Date,
});

export default mongoose.model("comments", commentSchema);