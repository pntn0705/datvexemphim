import mongoose from 'mongoose';

const BillSchema = mongoose.Schema({
    UserId : {type:mongoose.Types.ObjectId},
    Total: Number,
    TicketId : [{type:mongoose.Types.ObjectId}]
},
{
    timestamp: true
});

export default mongoose.model('bills', BillSchema);