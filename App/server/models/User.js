import mongoose from 'mongoose';
import jwt from 'jsonwebtoken';
import moment from 'moment';

const userSchema = mongoose.Schema({
    FullName: String,
    Email: {
        type: String,
        trim: true,
        unique: 1
    },
    Password: String,
    PhoneNumber: String,
    CMND: String,
    BirthDay: Date,
    Sex: String,
    Address: String,
    Avatar: String,
    CardNumber: String,
    Type: String,
    ActiveDate: Date,
    TotalSpend: Number,
    IsAdmin: Boolean,
    AuthenticationString: String,
    Exp: Date
},{
    timestamps: true
});

userSchema.methods.generateToken = (user, callback) => {
    let token = jwt.sign(user._id.toHexString(), 'num');
    let exp = moment().add(1, 'hour').valueOf();

    user.AuthenticationString = token;
    user.Exp = exp;
    user.save((err, user) => {
        if (err) return callback(err);
        callback(null, user);
    });
}

userSchema.statics.findByToken = (token, callback) => {
    let user = this;

    jwt.verify(token, 'num', (err, decode) => {
        user.findOne({'_id' : decode, AuthenticationString: token}, (err, user) => {
            if (err) return callback (err);
            callback(null, user);
        });
    })
}

const User = mongoose.model('users', userSchema);

export default User;