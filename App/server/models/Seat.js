import mongoose from 'mongoose';

const SeatSchema = mongoose.Schema({
    RoomId: {type:mongoose.Schema.Types.ObjectId},
    Column: Number,
    Row: String,
    IsDouble: Boolean,
});

export default mongoose.model('seats', SeatSchema);