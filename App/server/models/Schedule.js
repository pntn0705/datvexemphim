import mongoose from 'mongoose';

const scheduleSchema = mongoose.Schema({
    Day: String,
    Date: String,
    Time: Number,
    FilmId: {type:mongoose.Schema.Types.ObjectId},
    RoomId: {type:mongoose.Schema.Types.ObjectId},
});

export default mongoose.model('schedules', scheduleSchema);