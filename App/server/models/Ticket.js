import mongoose from 'mongoose';

const TicketSchema = mongoose.Schema({
    ScheduleId: {type:mongoose.Schema.Types.ObjectId},
    SeatId: {type:mongoose.Schema.Types.ObjectId},
    Status: String,
    Price: Number,
});

export default mongoose.model('tickets', TicketSchema);