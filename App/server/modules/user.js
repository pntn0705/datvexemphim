import moment from 'moment';
import UserModel from '../models/User.js';
import freeHandler from './freeHandler.js';
import emailService from './mailer.js';
import jwt from 'jsonwebtoken';

const UserHandler = {
    //arrow
    Login : (info, callback) => {
        UserModel.findOne({ Email: info.email }, (err, user) => {
            if (!user) return callback(false, 'Auth failed, email not found');

            if (user.Password != info.password) return callback(false, 'Auth failed, password not true');

            user.generateToken(user, (err, doc) => {
                if (err) return callback(false, err.message );

                callback(true, 'login succeed', doc.AuthenticationString );
            })
        })
    },

    CheckSession : (_token, callback) => {
        jwt.verify(_token, 'num', (err, decode) => {
            UserModel.findOne({'_id' : decode, AuthenticationString: _token}, (err, user) => {
                if (err) return callback (false);
                
                if (!user) return callback(false);

                let exp = moment(user.Exp);
                let now = moment(new Date());
                if (exp.diff(now) > 0)
                    callback(true);
                else callback(false);
            });
        })
    },

    Register : (info, callback) => {
        let s = freeHandler.RandomString(100);
        User.find({Email : info.Email}, (err, docs) => {
            if (err)
                return callback(err);
            
            if (docs.length == 0) {
                const user = new User({
                    FullName: info.FullName,
                    Email: info.Email,
                    Password: info.Password,
                    PhoneNumber: info.PhoneNumber,
                    CMND: info.CMND,
                    BirthDay: info.BirthDay,
                    Sex: info.Sex,
                    Address: info.Address,
                    Avatar: info.Avatar,
                    CardNumber: info.CardNumber,
                    Type: 'Bronze',
                    ActiveDate: null,
                    TotalSpend: null,
                    IsAdmin: info.IsAdmin,
                    IsActive: false,
                    AuthenticationString: s,
                });
        
                user.save((err, doc) => {
                    if(err)
                        return callback(err.message);

                    console.log("save successfully");
                    let emailOption = {
                        from: 'pntn070599@gmail.com',
                        to: info.Email,
                        subject: 'Active your account',
                        text: 'Please active your account at: http://localhost:69/Active/' + s
                    };
        
                    emailService.sendMail(emailOption, (err, info) => {
                        if (err)
                            return callback(err);
                        return callback('succeed');
                    });
                })
            }
            else{
                callback("Email is already exist");
            }
        })
    },

    ActiveAccount : (authenticationString, callback) => {
        let d = new Date();
        User.findOneAndUpdate({ AuthenticationString: authenticationString }, { $set : {IsActive : true, ActiveDate: "" + d.getDate() + (d.getMonth()+1) + d.getFullYear()}}, (err, doc) => {
            callback(err, doc);
        })
    },

    FindUser : (name, input, callback) => {
        switch (name) {
            case '_id':
                User.find({ _id : input }, (err, docs) => {
                    callback(err, docs);
                });
                break;
            case 'FullName': 
                User.find({ FullName : input }, (err, docs) => {
                    callback(err, docs);
                });
                break;
            case 'Password':
                User.find({ Password : input }, (err, docs) => {
                    callback(err, docs);
                });
                break;
            case 'PhoneNumber':
                User.find({ PhoneNumber : input }, (err, docs) => {
                    callback(err, docs);
                })
                break;
            case 'CMND':
                User.find({ CMND : input }, (err, docs) => {
                    callback(err, docs);
                })
                break;
            case 'BirthDay':
                User.find({ BirthDay : input }, (err, docs) => {
                    callback(err, docs);
                })
                break;
            case 'Sex':
                User.find({ Sex : input }, (err, docs) => {
                    callback(err, docs);
                })
                break;
            case 'Address':
                User.find({ Address : input }, (err, docs) => {
                    callback(err, docs);
                })
                break;
            case 'City':
                User.find({ City : input }, (err, docs) => {
                    callback(err, docs);
                })
                break;
            case 'Avatar':
                User.find({ Avatar : input }, (err, docs) => {
                    callback(err, docs);
                })
                break;
            case 'CardNumber':
                User.find({ CardNumber : input }, (err, docs) => {
                    callback(err, docs);
                })
                break;
            case 'Type':
                User.find({ Type : input }, (err, docs) => {
                    callback(err, docs);
                })
                break;
            case 'ActiveDate':
                User.find({ ActiveDate : input }, (err, docs) => {
                    callback(err, docs);
                })
                break;
            case 'TotalSpend':
                User.find({ TotalSpend : input }, (err, docs) => {
                    callback(err, docs);
                })
                break;
            case 'IsAdmin':
                User.find({ IsAdmin : input }, (err, docs) => {
                    callback(err, docs);
                })
                break;
            case 'AuthenticationString':
                User.find({ AuthenticationString : input }, (err, docs) => {
                    callback(err, docs);
                })
                break;
        }
        
    },

    GetAllUser : (callback) => {
        UserModel.find((err, docs) => callback(err, docs)); 
    }
}

export default UserHandler;