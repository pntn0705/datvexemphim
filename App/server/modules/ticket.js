import Ticket from '../models/Ticket.js';
import Room from '../models/Room.js';
import Seat from '../models/Seat.js';

const TicketHandler = {
    Generate : (data, callback) => {
        let ticket = new Ticket({
            ScheduleId: data.ScheduleId,
            SeatId: data.SeatId,
            Status: null,
            Price: data.Price
        });

        ticket.save((err, ticket) => {
            if (err) return callback ({ status: false, message: err.message })

            callback ({ status: true, message: 'succeed' });
        })
    },

    AutoGenerate : (data, callback) => {
        Room.findOne({ _id: data.RoomId }, (err, doc) => {
            if (err) return callback({ status: false, message: err.message });

            if (!doc) return callback({ status: false, message: 'Room not found'});

            Seat.find({ RoomId: doc._id }, (err, docs) => {
                if (err) return callback({ status: false, message: err.message });

                if (docs.length == 0) return callback({ status: false, message: 'Room have no seat'});

                docs.map((seat) => {
                    console.log(seat.Column, seat.Row, seat.IsDouble)
                    let price = 0;
                    if (seat.IsDouble == true)
                        price = 100000;
                    else price = 45000;
                
                        let ticket = new Ticket({
                            ScheduleId: data.ScheduleId,
                            SeatId: seat._id,
                            Status: null,
                            Price: price
                        });
                
                        ticket.save((err, ticket) => {
                            if (err) return callback ({ status: false, message: err.message })
                
                            callback ({ status: true, message: 'succeed' });
                        })
                    })
            })
        })
    }
}

export default TicketHandler;