import Schedule from '../models/Schedule.js';
import TicketHandler from './ticket.js';

const ScheduleHandle = {
    CreateNewSchedule : (data, callback) => {
        let schedule = new Schedule({
            Day: data.Day,
            Date: data.Date,
            Time: data.Time,
            FilmId: data.FilmId,
            RoomId: data.RoomId,
        })

        schedule.save((err, schedule) => {
            if (err) return callback({ status: false, message: err.message });
        
            TicketHandler.AutoGenerate({ ScheduleId: schedule._id, RoomId: schedule.RoomId }, (status, message) => {
                callback(status, message);
            })
        });
    },

    GetAll : (callback) => {
        let result = [];
        let t = new Date();
        let tmp = t.getHours() + "" + t.getMinutes();
        console.log(parseInt(tmp));
        Schedule.find({Time: { $gte : parseInt(tmp) }}, (err, docs) => {
            if (err) return callback(err);

            let string = (t.getMonth() + 1) + "/" + t.getDate() + "/" + t.getFullYear();

            docs.map(doc => {
                console.log(doc.Time, tmp);
                console.log(new Date(doc.Date).valueOf(), new Date(string).valueOf());
                if ((new Date(doc.Date).valueOf()) >= (new Date(string).valueOf()))
                    result.push(doc);
            })

            callback(null, result);
        })
    },

    GetByDate : (date, callback) => {
        let t = new Date();
        let tmp = t.getHours() + "" + t.getMinutes();
        console.log(date);
        Schedule.find({Date: date, Time: { $gte : parseInt(tmp) }}, (err, docs) => {
            if (err) return callback(false, err.message);

            if (docs.length === 0) return callback(false, 'no schedule in this date');
            callback(true, docs);
        });
    },

    GetByDateAndFilmId : (date, id, callback) => {
        let t = new Date();
        let tmp = t.getHours() + "" + t.getMinutes();
        console.log(id);
        console.log(date);
        Schedule.find({Date: date, FilmId: id, Time: { $gte : parseInt(tmp) }}, (err, docs) => {
            if (err) return callback(false, err.message);

            if (docs.length === 0) return callback(false, 'no schedule in this date');
            callback(true, docs);
        });
    }
}

export default ScheduleHandle;