const FreeHandler = {
    RandomString : (number) => {
        let input = "qwertyuiopasdfghjklzxcvbnm0123456789QWERTYUIOPASDFGHJKLZXCVBNM";
        let output = "";

        for (let i = 0; i < number; i++){
            output += input[Math.floor(Math.random() * input.length)];    
        }
        return output;
    },

    GetNowBySeconds : (callback) => {
        let d = new Date();
        let seconds = d.getSeconds() + 1;
        seconds += (d.getMinutes() + 1) * 60;
        seconds += (d.getHours() + 1) * 60 * 60;
        seconds += (d.getDate()) * 24 * 60 * 60;

        let month = d.getMonth() + 1;
        for (let i = 1; i < month; i++) {
            switch (i) {
                case 1:
                    seconds += 31 * 24 * 60 * 60;
                    return;
                case 2:
                    seconds += 28 * 24 * 60 * 60;
                    return;    
                case 3:
                    seconds += 31 * 24 * 60 * 60;
                    return;
                case 4:
                    seconds += 30 * 24 * 60 * 60;
                    return;
                case 5:
                    seconds += 31 * 24 * 60 * 60;
                    return;
                case 6:
                    seconds += 30 * 24 * 60 * 60;
                    return;
                case 7:
                    seconds += 31 * 24 * 60 * 60;
                    return;
                case 8:
                    seconds += 31 * 24 * 60 * 60;
                    return;
                case 9:
                    seconds += 30 * 24 * 60 * 60;
                    return;
                case 10:
                    seconds += 31 * 24 * 60 * 60;
                    return;
                case 11:
                    seconds += 30 * 24 * 60 * 60;
                    return;
                case 12:
                    seconds += 31 * 24 * 60 * 60;
                    return;
            }
        }

        seconds += d.getFullYear() * 31536000;
        console.log('thoi gian', seconds);
        callback(seconds);
    }
};

export default FreeHandler;