import Film from '../models/Film.js';
import UserHandler from './user.js';

const FilmHandler = {
    GetAll : (callback) => {
        Film.find((err, docs) => {
            if (err)
                return callback({status: false, message: err.message}, null);

            callback({status: true, message: 'succeed'}, docs);
        });
    },

    CreateNew : (data, callback) => {
        let newFilm = new Film({
            Name: data.Name,
            Directors: data.Directors,
            Casts: data.Casts,
            GENRE: data.GENRE,
            Running_Time: data.Running_Time,
            Language: data.Language,
            ReleaseDate: data.ReleaseDate,
            Details: data.Details,
            Trailer: data.Trailer,
            Image: data.Image
        });

        newFilm.save((err, doc) => {
            if (err)
                return callback({status: false, message: err.message}, null);

            callback({status: true, message: 'succeed'}, doc);
        });
    },

    GetById : (id, callback) => {
        Film.find({ _id : id }, (err, docs) => {
            if (err)
                return callback({status: false, message: err.message}, null);
            
            if (docs.length === 0)
                return callback({status: false, message: 'Something went wrong!, could not find this film'});

            callback({status: true, message: 'succeed'}, docs);
        });
    },

    Update : (data, callback) => {
        Film.findOneAndUpdate({_id : data._id}, { $set: {
            Name: data.Name,
            Directors: data.Directors,
            Casts: data.Casts,
            GENRE: data.GENRE,
            Running_Time: data.Running_Time,
            Language: data.Language,
            ReleaseDate: data.ReleaseDate,
            Details: data.Details,
            Trailer: data.Trailer,
        }}, (err, doc) => {
            if (err)
                return callback({status: false, message: err}, null);
            
            if (doc == null)
                return callback({status: false, message: 'Something went wrong!, could not find this film'});

            callback({status: true, message: 'succeed'}, data);
        });
    },

    Delete : (id, callback) => {
        Film.deleteOne({_id: id}, (err) => {
            if (err) return callback(err);

            callback(null, 'succeed');
        })
    }
}

export default FilmHandler;