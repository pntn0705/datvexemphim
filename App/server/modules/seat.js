import Seat from '../models/Seat';

const SeatHandler = {
    CreateNew : (data, callback) => {
        let newSeat = new Seat({
            RoomId: data.RoomId,
            Column: data.Column,
            Row: data.Row,
            IsDouble: data.IsDouble
        });

        newSeat.save((err, doc) => {
            if (err) return callback({ status: false, message: err.message}, null);

            callback({ status: true, message: 'succeed' }, doc);
        })
    },

    Update : (data, callback) => {
        Seat.findOneAndUpdate({ _id : data._id }, { $set: {
            RoomId: data.RoomId,
            Column: data.Column,
            Row: data.Row,
            IsDouble: data.IsDouble
        }}, (err, doc) => {
            if (err)
                return callback({status: false, message: err}, null);
            
            if (doc == null)
                return callback({status: false, message: 'Something went wrong!, could not find this seat'});

            callback({status: true, message: 'succeed'}, data);
        });
    },

    GetAll : (callback) => {
        Seat.find((err, docs) => {
            if (err) return callback({ status: false, message: err.message }, null);

            callback({ status: true, message: 'succeed' }, docs);
        })
    }
}

export default SeatHandler;