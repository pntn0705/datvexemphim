import Bill from '../models/Bill.js';

const BillHandler = {
    CreateNew : (data, callback) => {
        let newBill = new Bill({
            UserId : data.UserId,
            Total: data.Total,
            TicketId : data.Tickets
        });

        newBill.save((err, doc) => {
            if (err) return callback({status: false, message: err}, null);

            callback({status: false, message: 'succeed'}, doc);
        })
    },
    
    GetAll : (callback) => {
        Bill.find((err, docs) => {
            if (err) return callback({ status: false, message: err}, null);

            if (docs.length === 0) return callback({ status: false, message: 'cannot find this bill, please try again'}, null);

            callback({ status: true, message: 'succeed'}, docs);
        });
    },

    Delete : (id, callback) => {
        //Change ticket status


        Bill.findOneAndDelete({_id : id}, (err, doc) => {
            if (err) return callback({ status: false, message: err }, null);

            callback({ status: true, message: 'succeed' }, doc);
        });
    }
};

export default BillHandler;