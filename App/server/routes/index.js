import express from 'express';
import API from './api/index.js';
import UserHandler from '../modules/user.js';

const Router = express.Router();

Router.use('/api', API);

Router.get('/', (req, res) => {
    res.send('hello');
});

Router.get('/Active/:authenticationString', (req, res) => {
    UserHandler.ActiveAccount(req.params.authenticationString, (err, doc) => {
        if (err)
            return res.status(200).json({result: err});

        if(doc == null)
            return res.status(200).json({result: "Active false"});

        return res.status(200).json({result : doc});
    })
})

export default Router;