import express from 'express';
import ScheduleHandle from '../../modules/schedule.js';

const ScheduleRoute = express.Router();

ScheduleRoute.post('/CreateNewSchedule', (req, res) => {
    let tmp = new Date(req.body.Date);
    ScheduleHandle.CreateNewSchedule({
        Day: req.body.Day,
        Date: tmp.toDateString(),
        Time: req.body.Time,
        FilmId: req.body.FilmId,
        RoomId: req.body.RoomId
    }, (status, message) => {
        console.log(status, message);
    });

    res.json('succeed');
})

ScheduleRoute.get('/GetAllSchedule', (req, res) => {
    console.log('get schedule');
    ScheduleHandle.GetAll((err, docs) => {
        if (err) return res.status(500).send('something went wrong!');
        res.status(200).json(docs);
    })
})

ScheduleRoute.post('/GetByDate', (req, res) => {
    let tmp = new Date(req.body.Date).toDateString();
    ScheduleHandle.GetByDate(tmp, (status, result) => {
        if (status === false) return res.status(500).json(result);

        res.status(200).json(result);
    })
})

ScheduleRoute.post('/GetByDateAndFilmId', (req, res) => {
    let tmp = new Date(req.body.Date).toDateString();
    ScheduleHandle.GetByDateAndFilmId(tmp, req.body.Id, (status, result) => {
        if (status === false) return res.status(500).json(result);

        res.status(200).json(result);
    })
})

export default ScheduleRoute;