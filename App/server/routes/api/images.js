import express from 'express';
import { dirname } from 'path';
import path from 'path';
import { fileURLToPath } from 'url';

const __filename = fileURLToPath(import.meta.url);
const __dirname = dirname(__filename);

const Images = express.Router();

Images.get('/:name', (req, res) => {
    res.status(200).sendFile(path.resolve(__dirname + '../../../images/'+ req.params.name));
});

Images.get('/', (req, res) => {
    res.send('hello');
})

export default Images;