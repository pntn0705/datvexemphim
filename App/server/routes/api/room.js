import Room from '../../models/Room.js';
import express from 'express';
import Seat from '../../models/Seat.js';

const RoomRoute = express.Router();

RoomRoute.get('/init', (req, res) => {
    Room.deleteMany({}, (err) => {
        if (err) return res.status(200).json({status : false});
    })

    let room = new Room({
        Name: 'R1',
        TotalSeats: 44
    })

    room.save((err, doc) => {
        if (err) return res.status(200).json({ status: false, message: err.message });
    })

    let room2 = new Room({
        Name: 'R2',
        TotalSeats: 44
    })

    room2.save((err, doc) => {
        if (err) return res.status(200).json({ status: false, message: err.message });
    })

    let room3 = new Room({
        Name: 'R3',
        TotalSeats: 44
    })

    room3.save((err, doc) => {
        if (err) return res.status(200).json({ status: false, message: err.message });
    })

    let room4 = new Room({
        Name: 'R4',
        TotalSeats: 44
    })

    room4.save((err, doc) => {
        if (err) return res.status(200).json({ status: false, message: err.message });
    })

    for (let i = 1; i <= 10; i++) {
        let seatA = new Seat({
            RoomId: room._id,
            Column: i,
            Row: 'A',
            IsDouble: false,
        })

        seatA.save();

        let seatB = new Seat({
            RoomId: room._id,
            Column: i,
            Row: 'B',
            IsDouble: false,
        })

        seatB.save();

        let seatC = new Seat({
            RoomId: room._id,
            Column: i,
            Row: 'C',
            IsDouble: false,
        })

        seatC.save();

        let seatD = new Seat({
            RoomId: room._id,
            Column: i,
            Row: 'D',
            IsDouble: false,
        })

        seatD.save();
    }

    let seatE = new Seat({
        RoomId: room._id,
        Column: 1,
        Row: 'E',
        IsDouble: true,
    })

    seatE.save();

    seatE = new Seat({
        RoomId: room._id,
        Column: 2,
        Row: 'E',
        IsDouble: true,
    })

    seatE.save();

    seatE = new Seat({
        RoomId: room._id,
        Column: 3,
        Row: 'E',
        IsDouble: true,
    })

    seatE.save();

    seatE = new Seat({
        RoomId: room._id,
        Column: 4,
        Row: 'E',
        IsDouble: true,
    })

    seatE.save();

    res.status(200).json({status: 'succeed'});
})

RoomRoute.get('/GetAllRoom', (req, res) => {
    Room.find((err, rooms) => {
        if (err) return res.status(200).json({status: false});

        rooms.map(room)
    })
})

RoomRoute.get('/GetSeat/:name', (req, res) => {
    console.log('name', req.params.name);
    Room.findOne({Name: req.params.name}, (err, doc) => {
        if (err) return res.json(err.message);

        if (!doc) return res.json('false');

        Seat.find({RoomId: doc._id}, (err, seats) => {
            if (err) return res.json(err.message);

            res.json(seats);
        })
    })
})

export default RoomRoute;