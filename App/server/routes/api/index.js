import express from 'express';
import UserRoute from './user.js';
import Images from './images.js';
import Film from './film.js';
import moment from 'moment';
import Room from './room.js';
import Schedule from './schedule.js';

const Api = express.Router();

Api.use('/user', UserRoute);
Api.use('/image', Images);
Api.use('/film', Film);
Api.use('/room', Room);
Api.use('/schedule', Schedule);


export default Api;