import express from 'express';
import User from '../../models/User.js';
import UserHandler from '../../modules/user.js';
const UserRoute = express.Router();

UserRoute.post('/login', (req, res) => {
    console.log(res.body);
    let info = {
        email: req.body.email,
        password: req.body.password
    };
    UserHandler.Login(info, (status, message, w_auth) => {
        if (status == false) return res.status(200).json({ status: false, message: message })
        res.cookie('w_auth', w_auth).status(200).json({ status: true, message: message});
    });
});

UserRoute.get('/CheckMySession', (req, res) => {
    UserHandler.CheckSession(req.cookies.w_auth, (status) => {
        if (status === false) return res.status(200).json({ status: false });

        res.status(200).json({ status: true });
    })
});

UserRoute.post('/register', (req, res) => {
    const user = {
        FullName: req.body.FullName,
        Email: req.body.Email,
        Password: req.body.Password,
        PhoneNumber: req.body.PhoneNumber,
        CMND: req.body.CMND,
        BirthDay: new Date(req.body.BirthDay),
        Sex: req.body.Sex,
        Address: req.body.Address,
        Avatar: req.body.Avatar,
        CardNumber: null,
        Type: req.body.Type,
        ActiveDate: null,
        TotalSpend: null,
        IsAdmin: true,
        AuthenticationString: "",
        Exp: null
    };

    UserHandler.Register(user, (result) => res.send(result) );
});

UserRoute.get('/test', (req, res) => {
    const user = new User({
        FullName: "Pham Nguyen Truong Nam",
        Email: "pntn0705playgame@gmail.com",
        Password: "Cccc1254",
        PhoneNumber: "0918814602",
        CMND: "352564952",
        BirthDay: "07-05-1999",
        Sex: "Nam",
        Address: "An Giang",
        Avatar: null,
        CardNumber: null,
        Type: "admin",
        ActiveDate: null,
        TotalSpend: null,
        IsAdmin: true,
        AuthenticationString: "",
        Exp: null
    });

    user.save((err, doc) => {
        if (err) return res.json({status: false, message: err.message});

        res.json({ status: 'succeed', message: doc })
    })
});

UserRoute.get('/', function(req, res){
    UserHandler.FindUser('Email', 'pntn0705playgame@gmail.com', (err, docs) => {
        if (err) return res.status(200).json({ error: err});

        if (docs.length == 0) return res.status(200).json({ result: "info not true" });

        return res.status(200).json({ result: docs });
    })
 });

 UserRoute.get('/GetAllUser', (req, res) => {
     UserHandler.GetAllUser((err, docs) => {
         if (err) return res.status(200).json({error: err});
         return res.status(200).json(docs);
     })
 })

export default UserRoute;