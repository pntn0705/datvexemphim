import express from 'express';
import FilmHandler from '../../modules/film.js';

const FilmRouter = express.Router();

FilmRouter.get('/GetAllFilm/', (req, res) => {
    FilmHandler.GetAll((result, docs) => {
        if (result.status == false)
            return res.status(200).json({ message: result.message });

        res.status(200).json(docs);
    });    
});

FilmRouter.post('/CreateNew', (req, res) => {
    let data = {
        Name: req.body.Name,
        Directors: req.body.Directors,
        Casts: req.body.Casts,
        GENRE: req.body.GENRE,
        Running_Time: req.body.Running_Time,
        Language: req.body.Language,
        ReleaseDate: req.body.ReleaseDate,
        Details: req.body.Details,
        Trailer: req.body.Trailer,
        Image: req.body.Image
    };
    FilmHandler.CreateNew(data, (result, doc) => {
        if(result.status == false) 
            return res.status(200).json({message: result.message, data: null});
            
            res.status(200).json({message: result.message, data: doc});
    });
});

FilmRouter.post('/GetId', (req, res) => {
    FilmHandler.GetById(req.body.id, (result, docs) => {
        if (result.status == false)
            return res.status(200).json({message: result.message, data: null });
        
        res.status(200).json(docs);
    });
});

FilmRouter.post('/update', (req, res) => {
    let data = {
        _id : req.body.id,
        Name: req.body.Name,
        Directors: req.body.Directors,
        Casts: req.body.Casts,
        GENRE: req.body.GENRE,
        Running_Time: req.body.Running_Time,
        Language: req.body.Language,
        ReleaseDate: req.body.ReleaseDate,
        Details: req.body.Details,
        Trailer: req.body.Trailer
    };

    FilmHandler.Update(data, (result, doc) => {
        if (result.status == false)
            return res.status(200).json({message: result.message, data: null });

        res.status(200).json({message: result.message, data: doc });
    })
})

FilmRouter.post('/delete', (req, res) => {
    FilmHandler.Delete(req.body.Id, (err, message) => {
        if (err) return res.status(500).send('cannot delete');

        res.status(200).send(message);
    })
})

export default FilmRouter;