import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';
import './App.css';
import createMuiTheme from '@material-ui/core/styles/createMuiTheme';
import home from './pages/client-pages/home';
import { MuiThemeProvider } from '@material-ui/core';
import {Provider} from 'react-redux';
import store from './redux/store';
import jwtDecode from 'jwt-decode';
import axios from 'axios';
import AuthRoute from './util/AuthRoute';

import {SET_AUTHENTICATED} from './redux/types';

import movies from './pages/client-pages/movies';
import movieschedule from './pages/client-pages/movieschedule';
import cinema from './pages/client-pages/cinema';
import price from './pages/client-pages/price';
import promotions from './pages/client-pages/promotions';
import members from './pages/client-pages/members';
import signup from './pages/client-pages/signup';
import signin from './pages/client-pages/signin';
import main from './pages/management-pages/main';
import slidercontent from './pages/management-pages/slidercontent';
import user from './pages/management-pages/user';
import films from './pages/management-pages/films';

const theme = createMuiTheme({
  palette: {
    primary: {
      light: '#33c9dc',
      main: '#00bcd4',
      dark: '#008394',
      contrastText: '#fff',
    },
    secondary: {
      light: '#f73378',
      main: '#f50057',
      dark: '#ab003c',
      contrastText: '#fff',
    },
    typography:{
      useNextVariants: true
    }
  },
});

const token = sessionStorage.AuthenticationString;
if(token){
  console.log('da dang nhap');
  //gui req xuong server post : token

  //if con han sd thi dung`

  //else xoa'
}

class App extends Component {
  render(){
    return(
        <MuiThemeProvider theme={theme}>
          <div className="App">          
            <Provider store={store}>
              <Router>
                  <Route exact path="/" component={home}/>
                  <Route exact path="/phim" component={movies}/>
                  <Route exact path="/rapchieuphim" component={cinema}/>
                  <Route exact path="/lichchieuphim" component={movieschedule}/>
                  <Route exact path="/giave" component={price}/>
                  <Route exact path="/uudai" component={promotions}/>
                  <Route exact path="/thanhvien" component={members}/>
                  <AuthRoute exact path="/dangnhap" component={signin}/>
                  <Route exact path="/dangky" component={signup}/>
                  <Route exact path="/quanly" component={main}/>
                  <Route exact path="/quanly/slidercontent" component={slidercontent}/>
                  <Route exact path="/quanly/user" component={user}/>
                  <Route exact path="/quanly/films" component={films}/>                
              </Router>
            </Provider>
        </div>
        </MuiThemeProvider>
    );
  }
  
}

export default App;
