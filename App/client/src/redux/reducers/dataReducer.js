import {SET_FILMS, LOADING_DATA,SET_USERS, INSERT_FILM} from '../types'

const initialState = {
    films: [],
    users:[],
    loading: false
};

export default function(state = initialState,action){
    switch(action.type){
        case LOADING_DATA:
            return{
                ...state,
                loading:true
            };
        case SET_FILMS:
            return{
                ...state,
                films:action.payload,
                loading:false
            };
        case SET_USERS:
            return{
                ...state,
                users:action.payload,
                loading:false
            }
        case INSERT_FILM:
            return{
                ...state,
                films:[
                    action.payload,
                    ...state.films
                ]
            }
        default:
            return state;
    }
}