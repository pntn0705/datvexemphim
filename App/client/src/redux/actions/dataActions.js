import {SET_FILMS,SET_USERS,LOADING_DATA,INSERT_FILM,LOADING_UI} from '../types'
import axios from 'axios'

export const getFilms = () => dispatch =>{
    dispatch({type:LOADING_DATA});
    axios.get('/film/getallfilm')
        .then(res=>{
            dispatch({
                type:SET_FILMS,
                payload:res.data
            })
        })
        .catch(err=>{
            dispatch({
                type:SET_FILMS,
                payload:[]
            });
        });
};

export const getUsers = () => dispatch =>{
    dispatch({type:LOADING_DATA});
    axios.get('/user/getalluser')
        .then(res=>{
            dispatch({
                type:SET_USERS,
                payload:res.data
            })
        })
        .catch(err=>{
            dispatch({
                type:SET_USERS,
                payload:[]
            });
        });
};

export const insertFilm = (newFilm) => dispatch =>{
    dispatch({type:LOADING_DATA});
    axios.post('/film/CreateNew',newFilm)
    .then(res=>{
        dispatch({
            type:INSERT_FILM,
            payload:res.data
        })
    })
    .catch(err=>{
        console.log(err)
    })
}

export const getFilmByDateAndId = (data) => dispatch =>{
    dispatch({type: LOADING_DATA});
    axios.post('/schedule')
}