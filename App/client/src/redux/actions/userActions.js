import {SET_ERRORS,CLEAR_ERRORS, LOADING_UI, SET_UNAUTHENTICATED,LOADING_USER} from '../types';
import axios from 'axios';

export const loginUser = (userData,history) => (dispatch) =>{
    axios.post('user/login', userData)
        .then(res =>{
            console.log('token', res.data);
            setAuthorizationHeader(res.data.Token)
            // dispatch(getUserData());
            // dispatch({type: CLEAR_ERRORS});
            if(!res.data.Token)
                console.log('co cc');
            else
                history.push('/');
        })
        .catch(err =>{
            dispatch({
                type:SET_ERRORS,
                payload: err.response.data
            });
        });
};

const setAuthorizationHeader = (token) =>{    
    const FBIdToken = `${token}`
    sessionStorage.setItem('AuthenticationString', FBIdToken);
    axios.defaults.headers.common['Authorization'] = FBIdToken;
}