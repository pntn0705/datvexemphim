import React, { Component } from 'react';
import withStyles from '@material-ui/core/styles/withStyles'


import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Grid from '@material-ui/core/Grid';

import Sidebar from '../../components/management-components/Sidebar';

const styles = {
  container:{
    display: "flex"
  },
  content:{
    marginTop: 50
  },
  card:{
    margin:"30px 10px 10px 10px",
    width: 1266
  }
}

export class main extends Component {
  render() {
    const {classes} = this.props;
    return (
        <div className={classes.container}>
            <Sidebar/>
            <div className={classes.content}>
                <Grid container>
                    <Grid item sx={12}>
                      <Card className={classes.card}>
                          <CardContent>
                              <h1>Home</h1>
                          </CardContent>
                      </Card>
                    </Grid>
                </Grid>
            </div>
        </div>
    )
  }
}

export default withStyles(styles)(main)
