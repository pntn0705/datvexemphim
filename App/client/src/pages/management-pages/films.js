import React, { Component } from 'react';
import withStyles from '@material-ui/core/styles/withStyles';
import axios from 'axios';
import propTypes from 'prop-types'
import {connect} from 'react-redux';
import {getFilms,insertFilm} from '../../redux/actions/dataActions'


import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import TextField from '@material-ui/core/TextField';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import EditIcon from '@material-ui/icons/Edit';
import DeleteForeverIcon from '@material-ui/icons/DeleteForever';
import FolderIcon from '@material-ui/icons/Folder';
import AddCircleIcon from '@material-ui/icons/AddCircle';

import Sidebar from '../../components/management-components/Sidebar';

const styles = {
  container:{
    display: "flex"
  },
  content:{
    marginTop: 50
  },
  card:{
    margin:"30px 10px 10px 10px",
    width: 1270
  },
  button:{
    marginRight:10
  },
  textField:{
    marginRight:50
  },
  openFolder:{
    backgroundColor:"white",
    color:"#ffc400"
  },
  addNew:{
    float:"right",
    color:"white",
    backgroundColor:"#4caf50"
  },
  img:{
      width:70,
      height:100
  }
};

const StyledTableCell = withStyles((theme) => ({
  head: {
    backgroundColor: "gray",
    color: theme.palette.common.white,
  },
  body: {
    fontSize: 14,
  },
}))(TableCell);

export class films extends Component { 
  state = {
    Name:'',
    Directors:'',
    Casts:'',
    GENRE:'',
    Running_Time:'',
    ReleaseDate:'',
    Details:'',
    Trailer:'',
    Image:''
  }  
  componentDidMount(){
    this.props.getFilms();
  }
  handleImageChange = (event) => {
    const value = event.target.value;
    console.log(value);
    const files = event.target.files;
    console.log(files);
  };
  handleEditPicture = () => {
    const fileInput = document.getElementById('imageInput');
    fileInput.click();
  }

  handleSubmit = (event) =>{
      event.preventDefault();
      this.props.insertFilm({
          Name:this.state.Name,
          Directors:this.state.Directors,
          Language:this.state.Language,
          Casts:this.state.Casts,
          GENRE:this.state.GENRE,
          Running_Time:this.state.Running_Time,
          ReleaseDate:this.state.ReleaseDate,
          Details:this.state.Details,
          Trailer:this.state.Trailer,
          Image:this.state.Image
      });
  }
  handleChange = (event) => {
    this.setState({
        [event.target.name]:event.target.value
    });
}

  render() {
    const {classes} = this.props;
    const {films} = this.props.data;
    return (
        <div className={classes.container}>
            <Sidebar/>
            <div className={classes.content}>
                <Card className={classes.card}>
                    <CardContent>
                        <h2>Quản lý phim</h2>
                        <form className={classes.root} onSubmit={this.handleSubmit}>
                            <TextField name="Name" type="text" label="Tên phim" className={classes.textField}  onChange={this.handleChange}/>
                            <TextField name="Directors" type="text" label="Đạo diễn" className={classes.textField} onChange={this.handleChange}/>
                            <TextField name="Casts" type="text" label="Diễn viên" className={classes.textField}  onChange={this.handleChange}/>
                            <TextField name="GENRE" type="text" label="Thể loại" className={classes.textField}  onChange={this.handleChange}/>
                            <TextField name="Running_Time" type="text" label="Thời lượng" className={classes.textField}  onChange={this.handleChange}/>
                            <TextField name="Language" type="text" label="Ngôn ngữ" className={classes.textField} onChange={this.handleChange}/>
                            <TextField name="ReleaseDate" type="text" label="Khởi chiếu" className={classes.textField} onChange={this.handleChange}/>
                            <TextField name="Details" type="text" label="Mô tả" className={classes.textField} onChange={this.handleChange}/>
                            <TextField name="Trailer" type="text" label="Link Trailer" className={classes.textField} onChange={this.handleChange}/>
                            <TextField name="Image" type="text" label="Path ảnh" className={classes.textField} onChange={this.handleChange}/>
                            <input type="file" id="imageInput" hidden="hidden" onChange={this.handleImageChange}/>
                            <Button variant="contained" className={classes.openFolder} onClick={this.handleEditPicture}><FolderIcon fontSize="large"/></Button>
                            <Button type="submit" variant="contained" className={classes.addNew}><AddCircleIcon fontSize="large" /> Thêm mới</Button>
                        </form>
                        <br/>
                        <TableContainer component={Paper}>
                          <Table className={classes.table} aria-label="simple table">
                            <TableHead>
                              <TableRow>
                                <StyledTableCell align="left">Ảnh</StyledTableCell>
                                <StyledTableCell align="left">Tên phim</StyledTableCell>
                                <StyledTableCell align="left">Đạo diễn</StyledTableCell>
                                <StyledTableCell align="left">Diễn viên</StyledTableCell>
                                <StyledTableCell align="left">Thể loại</StyledTableCell>
                                <StyledTableCell align="left">Thời lượng</StyledTableCell>
                                <StyledTableCell align="left">Ngôn ngữ</StyledTableCell>
                                <StyledTableCell align="left">Khởi chiếu</StyledTableCell>
                                <StyledTableCell align="left">Mô tả</StyledTableCell>
                                <StyledTableCell align="left">Hành động</StyledTableCell>
                              </TableRow>
                            </TableHead>
                            <TableBody>
                              {films.map((films) => (
                                <TableRow key={films._id}>
                                  <TableCell align="left"><img src={films.Image} className={classes.img}/></TableCell>                                  
                                  <TableCell align="left">{films.Name}</TableCell>
                                  <TableCell align="left">{films.Directors}</TableCell>
                                  <TableCell align="left">{films.Casts}</TableCell>
                                  <TableCell align="left">{films.GENRE}</TableCell>
                                  <TableCell align="left">{films.Running_Time}</TableCell>
                                  <TableCell align="left">{films.Language}</TableCell>
                                  <TableCell align="left">{films.ReleaseDate}</TableCell>
                                  <TableCell align="left">{films.Details}</TableCell>
                                    <TableCell align="left">
                                    <Button variant="contained" color="primary" className={classes.button}><EditIcon/></Button>
                                    <Button variant="contained" color="secondary"><DeleteForeverIcon/></Button>
                                    </TableCell>
                                </TableRow>
                              ))}
                            </TableBody>
                          </Table>
                        </TableContainer>
                    </CardContent>
                </Card>
            </div>
        </div>
    )
  }
}

films.propTypes = {
    getFilms: propTypes.func.isRequired,
    insertFilm:propTypes.func.isRequired,
    data:propTypes.object.isRequired
}

const mapStateToProps = state =>({
    data:state.data,    
})

export default connect(mapStateToProps,{getFilms,insertFilm})(withStyles(styles)(films))

