import React, { Component } from 'react';
import withStyles from '@material-ui/core/styles/withStyles'


import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import TextField from '@material-ui/core/TextField';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import EditIcon from '@material-ui/icons/Edit';
import DeleteForeverIcon from '@material-ui/icons/DeleteForever';
import FolderIcon from '@material-ui/icons/Folder';
import AddCircleIcon from '@material-ui/icons/AddCircle';

import Sidebar from '../../components/management-components/Sidebar';

const styles = {
  container:{
    display: "flex"
  },
  content:{
    marginTop: 50
  },
  card:{
    margin:"30px 10px 10px 10px",
    width: 1270
  },
  button:{
    marginRight:10
  },
  textField:{
    marginRight:50
  },
  openFolder:{
    backgroundColor:"white",
    color:"#ffc400"
  },
  addNew:{
    float:"right",
    color:"white",
    backgroundColor:"#4caf50"
  }
};

const StyledTableCell = withStyles((theme) => ({
  head: {
    backgroundColor: "gray",
    color: theme.palette.common.white,
  },
  body: {
    fontSize: 14,
  },
}))(TableCell);

export class slidercontent extends Component {
  state = {
    data:[]
  };
  handleImageChange = (event) => {
    const value = event.target.value;
    console.log(value);
    const files = event.target.files;
    console.log(files);
  };
  handleEditPicture = () => {
    const fileInput = document.getElementById('imageInput');
    fileInput.click();
  }

  addItem(e){
    e.preventDefault();
    const {data} = this.state;
    var newData = {id:"1",name:"Ảnh 1",url:"cc"};
    this.setState({
      data:[...this.state.data,newData]
    })
  }

  render() {
    const {classes} = this.props;
    return (
        <div className={classes.container}>
            <Sidebar/>
            <div className={classes.content}>
                <Card className={classes.card}>
                    <CardContent>
                        <h2>Quản lý banner slider</h2>
                        <form className={classes.root} noValidate autoComplete="off" onSubmit={(e)=>{this.addItem(e)}}>
                            <TextField id="standard-basic" label="Tên ảnh" className={classes.textField}/>
                            <TextField id="standard-basic" label="Url" className={classes.textField}/>
                            <input type="file" id="imageInput" hidden="hidden" onChange={this.handleImageChange}/>
                            <Button variant="contained" className={classes.openFolder} onClick={this.handleEditPicture}><FolderIcon fontSize="large"/></Button>
                            <Button type="submit" variant="contained" className={classes.addNew}><AddCircleIcon fontSize="large" /> Thêm mới</Button>
                        </form>
                        <br/>
                        <TableContainer component={Paper}>
                          <Table className={classes.table} aria-label="simple table">
                            <TableHead>
                              <TableRow>
                                <StyledTableCell>ID</StyledTableCell>
                                <StyledTableCell align="left">Tên ảnh</StyledTableCell>
                                <StyledTableCell align="left">Url</StyledTableCell>
                                <StyledTableCell align="left">Hành động</StyledTableCell>
                              </TableRow>
                            </TableHead>
                            <TableBody>
                              {this.state.data.map((data) => (
                                <TableRow key={data.id}>
                                  <TableCell component="th" scope="row">
                                    {data.id}
                                  </TableCell>
                                  <TableCell align="left">{data.name}</TableCell>
                                  <TableCell align="left">{data.url}</TableCell>
                                    <TableCell align="left">
                                    <Button variant="contained" color="primary" className={classes.button}><EditIcon/></Button>
                                    <Button variant="contained" color="secondary"><DeleteForeverIcon/></Button>
                                    </TableCell>
                                </TableRow>
                              ))}
                            </TableBody>
                          </Table>
                        </TableContainer>
                    </CardContent>
                </Card>
            </div>
        </div>
    )
  }
}

export default withStyles(styles)(slidercontent)

