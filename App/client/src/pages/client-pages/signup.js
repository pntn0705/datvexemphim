import React, { Component,Fragment } from 'react';
import withStyles from '@material-ui/core/styles/withStyles';
import {Link} from 'react-router-dom';

import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';

import NavBar from '../../components/client-components/NavBar';
import Header from '../../components/client-components/Header';
import Footer from '../../components/client-components/Footer';

const styles = {
    root:{
        marginTop:20,
        marginBottom:20,
        textAlign:'center'
    },
    Textfield:{
        marginTop:20
    },
    button:{
        marginTop:20
    }
}

class signup extends Component {
    constructor(){
        super();
        this.state = {
            sex: '',
            checkB: true            
        }
    }
    handleChange = (event) => {
        this.setState({
            sex:event.target.value,
            checkB: event.target.checked
        });
    };
    render() {
        const {classes} = this.props;

        return (
            <Fragment>
                <Header/>
                    <div className="logo-header">
                        <h1><a color="inherit" href="/" className="logo-href">CINEMA</a></h1>
                    </div>
                <NavBar/>
                <Grid sx={12} container className={classes.root}>
                    <Grid sm={3} sx={12}>
                    </Grid>
                    <Grid sm={6} sx={12}>
                        <Card className="card">
                        <CardContent>
                            <form noValidate>
                                <TextField variant="outlined" id="name" name="name" type="text" label="Họ và tên" className={classes.Textfield} fullWidth/>
                                <TextField variant="outlined"id="email" name="email" type="email" label="Email" className={classes.Textfield} fullWidth/>
                                <TextField variant="outlined"id="password" name="password" type="password" label="Mật khẩu" className={classes.Textfield} fullWidth/>
                                <TextField variant="outlined"id="password" name="password" type="password" label="Xác nhận lại mật khẩu" className={classes.Textfield} fullWidth/>
                                <TextField
                                    variant="outlined"
                                    id="date"
                                    label="Ngày sinh"
                                    type="date"
                                    className={classes.Textfield}
                                    InputLabelProps={{
                                    shrink: true,
                                    }}
                                    fullWidth
                                />
                                <FormControl variant="outlined" className={classes.Textfield} fullWidth>
                                    <InputLabel id="demo-simple-select-outlined-label">Giới tính</InputLabel>
                                    <Select
                                    labelId="demo-simple-select-outlined-label"
                                    id="demo-simple-select-outlined"
                                    value={this.state.sex}
                                    onChange={this.handleChange}
                                    label="Giới tính"
                                    >
                                    <MenuItem value="Nam">Nam</MenuItem>
                                    <MenuItem value="Nữ">Nữ</MenuItem>
                                    <MenuItem value="Khác">Khác</MenuItem>
                                    </Select>
                                </FormControl>
                                <TextField variant="outlined"id="phone" name="phone" type="number" label="Số điện thoại" className={classes.Textfield} fullWidth/>                                
                                <FormControlLabel
                                    control={
                                    <Checkbox
                                        checked={this.state.checkedB}
                                        onChange={this.handleChange}
                                        name="checkedB"
                                        color="primary"
                                    />
                                    }
                                    label="Tôi cam kết tuân theo chính sách bảo mật và điều khoản sử dụng của Cinema."
                                />
                                <Button type="submit" variant="contained" color="primary"  className={classes.button}>Đăng ký                                         
                                </Button>
                                <br/>
                                <br/>
                                <small>Bạn đã có tài khoản ?<Link to="/dangnhap" className="link" > Đăng nhập</Link></small>
                            </form>
                        </CardContent>
                    </Card>                           
                    </Grid>
                    <Grid sm={3} sx={12}></Grid>
                </Grid>
                <Footer/>
            </Fragment>
        )
    }
}

export default withStyles(styles)(signup)
