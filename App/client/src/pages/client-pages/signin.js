import React, { Component,Fragment } from 'react';
import withStyles from '@material-ui/core/styles/withStyles';
import {Link} from 'react-router-dom';
import propTypes from 'prop-types';
import axios from 'axios';
import {connect} from 'react-redux';
import {loginUser} from '../../redux/actions/userActions';


import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import NavBar from '../../components/client-components/NavBar';
import Header from '../../components/client-components/Header';
import Footer from '../../components/client-components/Footer';

const styles = {
    root:{
        marginTop:20,
        marginBottom:20,
        textAlign:'center'
    },
    Textfield:{
        marginTop:20
    },
    button:{
        marginTop:20
    }
}

export class signin extends Component {
    constructor(){
        super();
        this.state = {
            email:'',
            password:'',            
        }        
    }
    handleTest = (event) => {
        event.preventDefault();
        let data = { AuthenticationString: sessionStorage.getItem('AuthenticationString')}
        axios.post('http://192.168.43.99:69/api/user/checkmysession', data)
        .then(res =>{
            console.log(res.data);
        })
    }

    handleSubmit = (event) =>{
        event.preventDefault();        
        const userData = {
            email: this.state.email,
            password: this.state.password
        };
        this.props.loginUser(userData,this.props.history);        
    };
    handleChange = (event) => {
        this.setState({
            [event.target.name]:event.target.value
        });
    }
    render() {
        const {classes} = this.props;
        return (
            <Fragment>
                <Header/>
                    <div className="logo-header">
                        <h1><a color="inherit" href="/" className="logo-href">CINEMA</a></h1>
                    </div>
                <NavBar/>
                <Grid sx={12} container className={classes.root}>
                    <Grid sm={3} sx={12}>
                    </Grid>
                    <Grid sm={6} sx={12}>
                        <Card className="card">
                        <CardContent>
                            <form noValidate onSubmit={this.handleSubmit}>
                                <TextField variant="outlined" id="email" name="email" type="email" label="Email" className={classes.Textfield} value={this.state.email} onChange={this.handleChange} fullWidth/>
                                <TextField variant="outlined"id="password" name="password" type="password" label="Mật khẩu" className={classes.Textfield} value={this.state.password} onChange={this.handleChange} fullWidth/>                                
                                <Button type="submit" variant="contained" color="primary"  className={classes.button}>Đăng nhập                                
                                </Button>
                                <br/>
                                <br/>
                                <small>Bạn không có tài khoản ?<Link to="/dangky" className="link" > Đăng ký</Link></small>
                                <Button onClick={this.handleTest}>click</Button>
                            </form>
                        </CardContent>
                    </Card>                           
                    </Grid>
                    <Grid sm={3} sx={12}></Grid>
                </Grid>
                <Footer/>
            </Fragment>
        )
    }
}

signin.propTypes = {
    classes:propTypes.object.isRequired,
    loginUser: propTypes.func.isRequired,
    user: propTypes.object.isRequired
}

const mapStateToProps = (state) =>({
    user:state.user
})

const mapActionsToProps = {
    loginUser
}

export default connect(mapStateToProps,mapActionsToProps)(withStyles(styles)(signin));
