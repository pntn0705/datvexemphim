import React, { Component,Fragment } from 'react';
import Grid from '@material-ui/core/Grid';
import NavBar from '../../components/client-components/NavBar';
import Header from '../../components/client-components/Header';
import Footer from '../../components/client-components/Footer';
import ScheduleBar from '../../components/client-components/ScheduleBar'; 
import MovieSchedule from '../../components/client-components/Schedule/MovieSchedule';

import {connect} from 'react-redux';
import {getFilms} from '../../redux/actions/dataActions';
import propTypes from 'prop-types';

export class movieschedule extends Component {
    componentDidMount(){
        this.props.getFilms();
    }
    render() {
        const {films} = this.props.data
        return (
            <Fragment>
                <Header/>
                    <div className="logo-header">
                        <h1><a color="inherit" href="/" className="logo-href">CINEMA</a></h1>
                    </div>
                <NavBar/>
                <div className="space"></div>
                <ScheduleBar/>
                <Grid container>
                    <Grid item sm={1} xs={12}>                            
                        </Grid>
                    <Grid sm={10} xs={12}>
                        {films.map((films)=>(
                            <MovieSchedule imageMovie={films.Image}
                            movieName={films.Name} types={films.GENRE} time={`Thời gian: ${films.Running_Time} phút`}
                        />
                        ))}
                    </Grid>
                    <Grid item sm={1} xs={12}>                            
                        </Grid>
                </Grid>                
                <Footer/>
            </Fragment>
        )
    }
}

movieschedule.propTypes = {
    getFilms: propTypes.func.isRequired,
    data:propTypes.object.isRequired
}

const mapStateToProps = state =>({
    data:state.data,    
})

export default connect(mapStateToProps,{getFilms})(movieschedule)
