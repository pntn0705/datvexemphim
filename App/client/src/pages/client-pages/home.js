import React, { Component,Fragment} from 'react'
import Slider from '../../components/client-components/Slider/Slider';
import images from '../../images';
import TabsBar from '../../components/client-components/TabsBar';
import MovieCard from '../../components/client-components/MovieCard';
import withStyles from '@material-ui/core/styles/withStyles';
import Grid from '@material-ui/core/Grid';
import NavBar from '../../components/client-components/NavBar';
import Header from '../../components/client-components/Header';
import Footer from '../../components/client-components/Footer';

import propTypes from 'prop-types'
import {connect} from 'react-redux';
import {getFilms} from '../../redux/actions/dataActions'


const styles = {
    child:{
        margin:'30px'
    }
}

export class home extends Component { 
    componentDidMount(){
        this.props.getFilms();
    }   
    render(){ 
    const {classes} = this.props;
    const{films} = this.props.data;
        return (
            <Fragment>
                <Header/>
                    <div className="logo-header">
                        <h1><a color="inherit" href="/" className="logo-href">CINEMA</a></h1>
                    </div>
                <NavBar/>
                <Slider slides={images} autoPlay={3}/>
                <div className="space"></div>              
                <TabsBar/>
                <Grid container>
                    <Grid item sm={1} xs={12}>
                            
                        </Grid>
                    <Grid sm={10} xs={12}>
                        <div className="flex-container">
                            {films.map((films)=>(
                                <MovieCard imageMovie={films.Image}
                                movieName={films.Name} types={films.GENRE} time={`Thời gian: ${films.Running_Time} phút`}/>    
                            ))}                            
                        </div>
                    </Grid>
                    <Grid item sm={1} xs={12}>
                            
                        </Grid>
                </Grid>
                <Footer/>
            </Fragment>
        )
    }
}

home.propTypes = {
    getFilms: propTypes.func.isRequired,
    data:propTypes.object.isRequired
}

const mapStateToProps = state =>({
    data:state.data,    
})

export default connect(mapStateToProps,{getFilms})(withStyles(styles)(home))
