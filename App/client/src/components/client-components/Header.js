import React, { Component } from 'react'

export class Header extends Component {
    render() {
        return (
            <div className="header">
                <div className="signin-signup">
                    <a className="login" href="/dangnhap">Đăng nhập</a>
                    <label className="middle">|</label>               
                    <a className="signup" href="/dangky">Đăng ký</a>
                </div>
            </div>
        )
    }
}

export default Header
