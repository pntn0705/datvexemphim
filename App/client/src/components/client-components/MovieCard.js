import React ,{Component} from 'react';
import withStyles from '@material-ui/core/styles/withStyles';
import clsx from 'clsx';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Collapse from '@material-ui/core/Collapse';
import Avatar from '@material-ui/core/Avatar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import { red } from '@material-ui/core/colors';
import FavoriteIcon from '@material-ui/icons/Favorite';
import ShareIcon from '@material-ui/icons/Share';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import MoreVertIcon from '@material-ui/icons/MoreVert';

const styles = {
  root: {
    maxWidth: 345,
    margin:25
  },
  media: {
    height: 0,
    paddingTop: '100%', // 16:9
  },
  img:{
    width:230,
    height:320
  }
}

class MovieCard extends Component {
  constructor(props){
    super(props);
    this.state = {
      imageMovie:'',
      movieName:'',
      types:'',
      time:''
    }
  }
  render(){
    const {classes} = this.props;
    return (
      <Card className={classes.root}>      
        <img src={this.props.imageMovie} alt="cc" className={classes.img}/>
        <CardContent>
          <Typography variant="h6" color="textSecondary" component="p">
            {this.props.movieName}
          </Typography>
          <Typography variant="body2" color="textSecondary" component="p">
            {this.props.types}
          </Typography>
          <Typography variant="body2" color="textSecondary" component="p">
            {this.props.time}
          </Typography>
        </CardContent>     
      </Card>
    );
  }
}

export default withStyles(styles)(MovieCard)