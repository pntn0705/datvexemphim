import React, { Component } from 'react';
import Grid from '@material-ui/core/Grid';

export class Footer extends Component {
    render() {
        return (
            <div className="footer">
                <Grid container>
                    <Grid sm={2} sx={12} className="info">
                        <h2>CINEMA</h2>
                        <a href="">Giới Thiệu</a>
                        <br/>
                        <a href="">Tiện Ích Online</a>
                        <br/>
                        <a href="">Thẻ Quà Tặng</a>
                        <br/>
                        <a href="">Tuyển Dụng</a>
                        <br/>
                        <a href="">Điều khoản bảo mật</a>
                        <br/>
                    </Grid>
                    <Grid sm={3} sx={12} className="info">
                    <h4>Điều khoản sử dụng</h4>
                        <a href="">Giới Thiệu</a>
                        <br/>
                        <a href="">Tiện Ích Online</a>
                        <br/>
                        <a href="">Thẻ Quà Tặng</a>
                        <br/>
                        <a href="">Tuyển Dụng</a>
                        <br/>
                        <a href="">Điều khoản bảo mật</a>
                        <br/>
                    </Grid>
                    <Grid sm={3} sx={12}>
                    <h4>Kết nối với chúng tôi</h4>
                        
                    </Grid>
                    <Grid sm={2} sx={12}>
                    <h4>Chăm sóc khách hàng</h4>
                        <p>Hotline: 1900 6017</p>
                        <p>Giờ làm việc: 8:00 - 22:00 (Tất cả các ngày bao gồm cả Lễ Tết)</p>
                        <p>Email hỗ trợ: <a href="">cinema@gmail.com</a></p>
                    </Grid>
                </Grid>
            </div>
        )
    }
}

export default Footer
