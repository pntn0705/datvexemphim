import React ,{Component} from 'react';
import withStyles from '@material-ui/core/styles/withStyles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid'
import LocalOfferIcon from '@material-ui/icons/LocalOffer';
import AvTimerIcon from '@material-ui/icons/AvTimer';


const styles = {
    root: {
        maxWidth: "100%",
        margin:25
      },
      media: {
        height: 0,
        paddingTop: '100%', // 16:9
      },
      img:{
        width:300,
        height:420
      }
}

class MovieSchedule extends Component {
  constructor(props){
    super(props);
    this.state = {
      imageMovie:'',
      movieName:'',
      types:'',
      time:''
    }
  }
  render(){
    const {classes} = this.props;
    return (
      <Card className={classes.root}>
          <Grid container>
              <Grid item sm={4} sx={12}>
                <img src={this.props.imageMovie} alt="cc" className={classes.img}/>
              </Grid>
              <Grid item sm={8} sx={12}>
              <CardContent>
                    <Typography variant="h4" color="textSecondary" component="p">
                        {this.props.movieName}
                    </Typography>
                    <Typography variant="body1" color="textSecondary" component="p">
                        <LocalOfferIcon/>{this.props.types} <AvTimerIcon/>{this.props.time}
                    </Typography>
                </CardContent>
              </Grid>
          </Grid>             
      </Card>
    );
  }
}

export default withStyles(styles)(MovieSchedule)