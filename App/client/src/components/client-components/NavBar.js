import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import ClickAwayListener from '@material-ui/core/ClickAwayListener';
import Grow from '@material-ui/core/Grow';
import Link from 'react-router-dom/Link';

import Paper from '@material-ui/core/Paper';
import Popper from '@material-ui/core/Popper';
import MenuItem from '@material-ui/core/MenuItem';
import MenuList from '@material-ui/core/MenuList';
import Grid from '@material-ui/core/Grid';

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    display: 'none',
    [theme.breakpoints.up('sm')]: {
      display: 'block',
    },
  },
}));

export default function ButtonAppBar() {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <AppBar position="static">
            <Toolbar className="nav-container">                
                <Button color="inherit" component={Link} to="/lichchieuphim">Lịch chiếu phim</Button>
                <Button color="inherit" component={Link} to="/phim">Phim</Button>
                <Button color="inherit" component={Link} to="/rapchieuphim">Rạp chiếu phim</Button>
                <Button color="inherit" component={Link} to="/giave">Giá vé</Button>
                <Button color="inherit" component={Link} to="/uudai">Ưu đãi</Button>
                <Button color="inherit" component={Link} to="/thanhvien">Thành viên</Button>               
            </Toolbar>
      </AppBar>
    </div>
  );
}